# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: npatton <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/17 08:12:07 by npatton           #+#    #+#              #
#    Updated: 2018/10/12 16:23:52 by npatton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	libftprintf.a

SRC		=	src/*/*.c src/*/*/*.c

OBJ		=	*.o

INC		=	include

CFLAGS	=	-Wall -Werror -Wextra

CC		=	gcc

all: 		$(NAME)

$(NAME):
			$(CC) $(CFLAGS) -c $(SRC) -I $(INC)
			ar -rc $(NAME) $(OBJ)
			ranlib $(NAME)

clean:
			rm -rf $(OBJ)

fclean:		clean
			rm -rf $(NAME)

re:			fclean all

norm:
			norminette $(SRC)

c:			clean

f:			fclean

r:			re

n:			norm

.PHONY: 	all $(NAME) clean fclean re norm c f r n
.SILENT:	all $(NAME) clean fclean re norm c f r n
