/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_convert_u(t_var *v, va_list args)
{
	char	*tmp;

	v->type = 'u';
	if (v->l == 1)
		tmp = ft_ltoa(va_arg(args, unsigned long));
	else if (v->l == 2)
		tmp = ft_ltoa(va_arg(args, unsigned long long));
	else if (v->h == 1)
		tmp = ft_ltoa((unsigned short)va_arg(args, unsigned int));
	else if (v->h == 2)
		tmp = ft_ltoa((unsigned char)va_arg(args, unsigned int));
	else if (v->j == 1)
		tmp = ft_ltoa(va_arg(args, uintmax_t));
	else if (v->z == 1)
		tmp = ft_ltoa(va_arg(args, size_t));
	else
		tmp = ft_itoa(va_arg(args, unsigned int));
	ft_print_flag(tmp, v);
	free(tmp);
}
