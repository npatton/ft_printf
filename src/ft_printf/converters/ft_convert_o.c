/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_o.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_convert_o(t_var *v, va_list args)
{
	char	*tmp;

	v->type = 'o';
	if (v->l == 1)
		tmp = ft_ltoabase(va_arg(args, unsigned long), 8);
	else if (v->l == 2)
		tmp = ft_ltoabase(va_arg(args, unsigned long long), 8);
	else if (v->h == 1)
		tmp = ft_ltoabase((unsigned short)va_arg(args, unsigned int), 8);
	else if (v->h == 2)
		tmp = ft_ltoabase((unsigned char)va_arg(args, unsigned int), 8);
	else if (v->j == 1)
		tmp = ft_ltoabase(va_arg(args, uintmax_t), 8);
	else if (v->z == 1)
		tmp = ft_ltoabase(va_arg(args, size_t), 8);
	else
		tmp = ft_ltoabase(va_arg(args, unsigned int), 8);
	ft_print_flag(tmp, v);
	free(tmp);
}
