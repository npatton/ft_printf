/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_i.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 15:43:46 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_convert_i(t_var *v, va_list args)
{
	char	*tmp;

	v->type = 'i';
	if (v->l == 1)
		tmp = ft_ltoa(va_arg(args, long));
	else if (v->l == 2)
		tmp = ft_ltoa(va_arg(args, long long));
	else if (v->h == 1)
		tmp = ft_ltoa((short)va_arg(args, int));
	else if (v->h == 2)
		tmp = ft_ltoa((char)va_arg(args, int));
	else if (v->j == 1)
		tmp = ft_ltoa(va_arg(args, intmax_t));
	else if (v->z == 1)
		tmp = ft_ltoa(va_arg(args, ssize_t));
	else
		tmp = ft_ltoa(va_arg(args, int));
	if (*tmp == '-')
		v->neg = 1;
	ft_print_flag(tmp, v);
	free(tmp);
}
