/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_convert_c(t_var *v, va_list args)
{
	char	*tmp;

	v->type = 's';
	tmp = (char *)malloc(sizeof(char) * 2);
	tmp[0] = va_arg(args, int);
	tmp[1] = '\0';
	if (tmp[0])
		ft_print_flag(tmp, v);
	free(tmp);
}
