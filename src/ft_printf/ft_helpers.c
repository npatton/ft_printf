/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_helpers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 20:18:24 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 15:43:49 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_print_vars(t_var v)			// Remove
{
	printf("\n\n/-------------------------------\\\n");
	printf("|\tplus:\t\t%i\t|\n", v.plus);
	printf("|-------------------------------|\n");
	printf("|\tleft:\t\t%i\t|\n", v.left);
	printf("|-------------------------------|\n");
	printf("|\twide:\t\t%i\t|\n", v.wide);
	printf("|-------------------------------|\n");
	printf("|\tprec:\t\t%i\t|\n", v.prec);
	printf("|-------------------------------|\n");
	printf("|\thash:\t\t%i\t|\n", v.hash);
	printf("|-------------------------------|\n");
	printf("|\tzero:\t\t%i\t|\n", v.zero);
	printf("|-------------------------------|\n");
	printf("|\tspac:\t\t%i\t|\n", v.spac);
	printf("|-------------------------------|\n");
	printf("|\ttype:\t\t%c\t|\n", v.type);
	printf("|-------------------------------|\n");
	printf("|\tleng:\t\t%i\t|\n", v.leng);
	printf("|-------------------------------|\n");
	printf("|\tneg:\t\t%i\t|\n", v.neg);
	printf("|-------------------------------|\n");
	printf("|\tl:\t\t%i\t|\n", v.l);
	printf("|-------------------------------|\n");
	printf("|\th:\t\t%i\t|\n", v.h);
	printf("|-------------------------------|\n");
	printf("|\tj:\t\t%i\t|\n", v.j);
	printf("|-------------------------------|\n");
	printf("|\tz:\t\t%i\t|\n", v.z);
	printf("|-------------------------------|\n");
	printf("|\ts:\t\t%i\t|\n", v.s);
	printf("\\-------------------------------/\n");
}

void		ft_add_flag(int *f, t_var *v, const char *fmt)
{
	if (ft_strchr(" -+0#", fmt[*f]))
	{
		if (fmt[*f] == '#')
			v->hash = 1;
		if (fmt[*f] == '-')
			v->left = 1;
		if (fmt[*f] == '+')
			v->plus = 1;
		if (fmt[*f] == '0')
			v->zero = 1;
		if (fmt[*f] == ' ')
			v->spac = 1;
	}
}

void		ft_add_leng(int *f, t_var *v, const char *fmt)
{
	if (ft_strchr("lhjz", fmt[*f]))
	{
		if (fmt[*f] == 'l' && v->l < 2 && !v->h && !v->j && !v->z)
			v->l++;
		if (fmt[*f] == 'h' && v->h < 2 && !v->l && !v->j && !v->z)
			v->h++;
		if (fmt[*f] == 'j' && !v->l && !v->h && !v->z)
			v->j++;
		if (fmt[*f] == 'z' && !v->l && !v->h && !v->j)
			v->z++;
	}
}

int			ft_fmt_nums(int *f, const char *fmt, va_list args)
{
	int		len;

	len = 0;
	if (fmt[*f] == '*')
	{
		len = va_arg(args, long long);
		(*f)++;
	}
	else
	{
		while (ft_isdigit(fmt[*f]))
			len = (len * 10) + (fmt[(*f)++] - '0');
	}
	return (len);
}

int			ft_get_wide(t_var *v)
{
	int		len;

	len = 0;
	if (v->prec)
	{
		if (v->type == 's' && v->prec >= v->leng)
			len = v->wide - v->leng;
		else if (v->prec >= v->wide)
			len = 0;
		else if (ft_strchr("ipoxX", v->type) && v->prec >= v->leng)
			len = v->wide - v->prec;
		else if (ft_strchr("ipoxX", v->type) && v->leng >= v->prec)
			len = v->wide - v->leng;
		else if (v->leng >= v->prec)
			len = v->wide - v->prec;
	}
	else
		len = v->wide - v->leng;
	if ((v->type == 'i' && !v->neg && (v->plus || v->spac)) ||
		(v->type == 'o' && v->hash))
		len--;
	if (ft_strchr("xX", v->type) && v->hash)
		len -= 2;
	return (len);
}
