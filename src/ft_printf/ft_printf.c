/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 13:01:05 by npatton           #+#    #+#             */
/*   Updated: 2018/08/10 11:06:58 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_fdprintf(const char *format, va_list args)
{
	t_var	v;
	int		*f;
	int		ret;

	if (!format)
		return (0);
	ret = 0;
	f = (int*)malloc(sizeof(int));
	*f = 0;
	v = ft_init_var();
	while (format[(*f)])
	{
		v = ft_init_var();
		if (format[*f] == '%')
		{
			if (!format[++(*f)])
				break ;
			ret += ft_parse_fmts(f, v, format, args);
		}
		else
			ret += write(1, &format[(*f)++], 1);
	}
	free(f);
	return (ret);
}

int			ft_printf(const char *format, ...)
{
	va_list	args;
	int		ret;

	ret = 0;
	if (!format)
		return (0);
	va_start(args, format);
	ret = ft_fdprintf(format, args);
	va_end(args);
	return (ret);
}
