/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 20:18:24 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 15:43:49 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_print_flag(char *str, t_var *v)
{
	v->leng = ft_strlen(str);
	if (v->type == 'i' && !v->neg && v->zero && v->plus)
		v->s += write(1, "+", 1);
	if ((ft_strchr("oxX", v->type) && v->hash && v->zero) || v->type == 'p')
		ft_print_zx(v);
	if (!v->left)
		ft_print_wide(v);
	if (v->type == 'i' && !v->neg && !v->zero && v->plus)
		v->s += write(1, "+", 1);
	else if (v->type == 'i' && !v->neg && v->spac)
		v->s += write(1, " ", 1);
	if (ft_strchr("poxX", v->type) && v->hash && !v->zero)
		ft_print_zx(v);
	ft_print_prec(str, v->prec, v);
	if (v->left)
		ft_print_wide(v);
}

void		ft_print_zx(t_var *v)
{
	if (v->type == 'p')
		v->s += write(1, "0x", 2);
	if (v->type == 'x')
		v->s += write(1, "0x", 2);
	if (v->type == 'X')
		v->s += write(1, "0X", 2);
	if (v->type == 'o')
		v->s += write(1, "0", 1);
}

void		ft_print_numprefix(t_var *v)
{
	int			len;

	len = v->prec - v->leng;
	if (v->neg && !v->left)
		v->s += write(1, "-", 1);
	while (len--)
		v->s += write(1, "0", 1);
}

void		ft_print_wide(t_var *v)
{
	int		len;

	if ((v->wide > v->leng && !v->prec) || (v->wide > v->prec && v->prec) ||
		(v->type == 's' && v->prec >= v->leng && v->prec >= v->wide))
	{
		len = ft_get_wide(v);
		if (v->zero && !v->left)
		{
			if (v->neg)
				v->s += write(1, "-", 1);
			while (len--)
				v->s += write(1, "0", 1);
		}
		else
		{
			while (len--)
				v->s += write(1, " ", 1);
		}
	}
}

void		ft_print_prec(char *str, int p, t_var *v)
{
	if (v->zero && v->neg && !v->left && (v->wide > v->leng || p > v->leng))
		str++;
	if (v->prec)
	{
		if (v->type == 's')
		{
			while (p-- && *str)
				v->s += write(1, str++, 1);
		}
		else if (ft_strchr("iopuxX", v->type))
		{
			if (v->prec > v->leng)
				ft_print_numprefix(v);
			while (*str)
				v->s += write(1, str++, 1);
		}
	}
	else
	{
		while (*str)
			v->s += write(1, str++, 1);
	}
}
