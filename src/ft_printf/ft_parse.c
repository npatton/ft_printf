/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 20:18:15 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 14:07:22 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_parse_fmts(int *f, t_var v, const char *fmt, va_list args)
{
	if (!fmt[*f])
		return (0);
	ft_parse_flag(f, &v, fmt);
	ft_parse_wide(f, &v, fmt, args);
	ft_parse_prec(f, &v, fmt, args);
	ft_parse_flag(f, &v, fmt);
//	write(1, "|", 1);						// Remove
	ft_parse_type(f, &v, fmt, args);
//	write(1, "|", 1);						// Remove
//	ft_print_vars(v);						// Remove
	return (v.s);
}

void		ft_parse_flag(int *f, t_var *v, const char *fmt)
{
	while (ft_strchr(" -+0#lhjz", fmt[*f]))
	{
		if (!fmt[*f])
			return ;
		ft_add_flag(f, v, fmt);
		ft_add_leng(f, v, fmt);
		ft_add_flag(f, v, fmt);
		(*f)++;
	}
}

void		ft_parse_wide(int *f, t_var *v, const char *fmt, va_list args)
{
	if (!fmt[*f])
		return ;
	v->wide = ft_fmt_nums(f, fmt, args);
}

void		ft_parse_prec(int *f, t_var *v, const char *fmt, va_list args)
{
	if (!fmt[*f])
		return ;
	if (fmt[*f] == '.')
	{
		(*f)++;
		if (!fmt[*f])
			return ;
		v->prec = ft_fmt_nums(f, fmt, args);
	}
}

void		ft_parse_type(int *f, t_var *v, const char *fmt, va_list args)
{
	if (ft_strchr("%cCidDoOpsSuUxX", fmt[*f]))
	{
		if (!fmt[*f])
			return ;
		else if (fmt[*f] == '%')
			ft_convert_p(v);
		else if (ft_strchr("oO", fmt[*f]))
			ft_convert_o(v, args);
		else if (ft_strchr("pP", fmt[*f]))
			ft_convert_m(v, args);
		else if (ft_strchr("uU", fmt[*f]))
			ft_convert_u(v, args);
		else if (ft_strchr("idD", fmt[*f]))
			ft_convert_i(v, args);
		else if (ft_strchr("cC", fmt[*f]))
			ft_convert_c(v, args);
		else if (ft_strchr("sS", fmt[*f]))
			ft_convert_s(v, args);
		else if (ft_strchr("xX", fmt[*f]))
			ft_convert_x(v, args, fmt[*f]);
		(*f)++;
	}
}
