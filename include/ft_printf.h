/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 13:01:36 by npatton           #+#    #+#             */
/*   Updated: 2018/08/09 19:07:23 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <locale.h>
# include <stdint.h>
# include <stdio.h>							// Remove

/*
**				Structs
*/

typedef struct	s_var
{
	int			plus;
	int			left;
	int			wide;
	int			prec;
	int			hash;
	int			zero;
	int			spac;
	char		type;
	int			leng;
	int			neg;
	int			l;
	int			h;
	int			j;
	int			z;
	int			s;
	int			w;
}				t_var;

/*
**				Wrappers
*/

int				ft_printf(const char *format, ...);
int				ft_fdprintf(const char *format, va_list args);

/*
**				Initializers
*/

t_var			ft_init_var(void);

/*
**				Parsers
*/

int				ft_parse_fmts(int *f, t_var v, const char *fmt, va_list args);
void			ft_parse_flag(int *f, t_var *v, const char *fmt);
void			ft_parse_wide(int *f, t_var *v, const char *fmt, va_list args);
void			ft_parse_prec(int *f, t_var *v, const char *fmt, va_list args);
void			ft_parse_type(int *f, t_var *v, const char *fmt, va_list args);

/*
**				Converters
*/

void			ft_convert_p(t_var *v);
void			ft_convert_o(t_var *v, va_list args);
void			ft_convert_m(t_var *v, va_list args);
void			ft_convert_u(t_var *v, va_list args);
void			ft_convert_i(t_var *v, va_list args);
void			ft_convert_c(t_var *v, va_list args);
void			ft_convert_s(t_var *v, va_list args);
void			ft_convert_x(t_var *v, va_list args, char c);

/*
**				Printers
*/

void			ft_print_vars(t_var v);			// Remove
void			ft_print_wide(t_var *v);
void			ft_print_zx(t_var *v);
void			ft_print_numprefix(t_var *v);
void			ft_print_prec(char *str, int p, t_var *v);
void			ft_print_flag(char *str, t_var *v);

/*
**				Helpers
*/

void			ft_add_flag(int *f, t_var *v, const char *fmt);
void			ft_add_leng(int *f, t_var *v, const char *fmt);
int				ft_fmt_nums(int *f, const char *fmt, va_list args);
int				ft_get_wide(t_var *v);

#endif
